# app.py
from flask import Flask, flash, request, redirect, url_for, render_template, jsonify
import urllib.request
import clasificacion_01
import base64
import json
import firebase
import os
from werkzeug.utils import secure_filename
from flask_ngrok import run_with_ngrok

def listToString(s):
    # initialize an empty string
    str1 = ""

    # traverse in the string
    for ele in s:
        str1 += ele

        # return string
    return str1

app = Flask(__name__)
run_with_ngrok(app)

UPLOAD_FOLDER = 'static/uploads/'

app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/', methods=['POST'])
def upload_image():
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
        flash('No image selected for uploading')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        # print('upload_image filename: ' + filename)
        flash('Image successfully uploaded and displayed below')
        # Pass image bytes to classifier
        results = clasificacion_01.analyse(UPLOAD_FOLDER + filename)
        print(results)
        # Return results as neat JSON object, using
        result = jsonify(results)
        print(result.json)

        response_data = result.json
        print(response_data)

        flash('Trash')
        for res in results:
            flash(res)

        #db = firebase.Firebase()
        #db.authenticate()
        #db.push(response_data)
        #print("Updated Firebase.")

        return render_template('index.html', filename=filename)
    else:
        flash('Allowed image types are - png, jpg, jpeg, gif')
        return redirect(request.url)


@app.route('/display/<filename>')
def display_image(filename):
    # print('display_image filename: ' + filename)
    return redirect(url_for('static', filename='uploads/' + filename), code=301)


if __name__ == "__main__":
    app.run()